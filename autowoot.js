/**
 * AutoWoot Script for PlugDJ.
 * @author Zercle Coding Co., Ltd.
 */
header("content-type: application/javascript");
function AutoWoot() {
	atw = {
		autowoot : true,
		clicks : 0,
		version : 2.01,
		close : function() {
			API.off(API.DJ_ADVANCE, atw.djAdvance);
			API.off(API.CHAT, atw.chat);
			$('#woot').unbind('click', atw.doubleClick);
		},
		djAdvance : function() {
			if (atw.autowoot) {
				setTimeout(function() {
					$("#woot").click();
				}, 2000);
			}
		},
		doubleClick : function() {
			atw.clicks++;
			if (atw.clicks == 2) {
				atw.autowoot = !atw.autowoot;
				atw.clicks = 0;
				API.chatLog(atw.autowoot ? 'AutoWoot is now on' : 'AutoWoot is now off');
			}
			setTimeout(function() {
				atw.clicks = 0;
			}, 600);
		}
	};

	API.on(API.ADVANCE, atw.djAdvance, this);
	$("#woot").bind('click', atw.doubleClick);

	API.chatLog("AutoWoot by Zercle v" + atw.version + ": http://apps.zercle.com/autowoot/");
	$('#woot').click();
}

if ( typeof atw !== "undefined")
	atw.close();

AutoWoot();
